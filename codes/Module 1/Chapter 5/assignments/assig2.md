> # **Gas Liquid Absorption**

*This exercise deals with the mass transfer operation known as gas absorption in which a soluble gas is absorbed from its mixture with an inert gas by means of a liquid in which the solute gas is more or less soluble.*

<br>
A common apparatus used in gas absorption is packed tower, an example of which is shown in figure (1). The device consists of a cylindrical column, or tower, equipped with a gas inlet and distributing space at the bottom; a liquid inlet and a distributor at top; gas and liquid outlets at the top and bottom respectively; and a supported mass of inert solid shapes, called tower packing.

<br>



![Image1](image1.jpg)
![Image2](image2.jpg)

<br>

The inlet liquid which may be pure solvent or a dilute solution of some solute (may or may not be same gas) in the solvent is distributed over the top of the packing by distributor and, in ideal operation, uniformly wets the surfaces of the packing. The solute containing gas enters the distributing space below the packing and flows upward through the interstices in the packing counter current to the flow of liquid. The packing provides large area of contact between liquid and gas and encourages intimate contact between phases. At ideal steady state condition, for a specified flow rates of liquid and gas phase, the extent of absorption will not changed with passage of time. In case of absorption in semi-batch process as shown in figure (2), the extent of absorption increases with passage of time as more and more gas is passed through the same liquid.

 <br>

The absorption column in the experimental setup was packed with raschig rings. They provide a large surface area within the volume of the column for the interaction between liquid and gas and also enhance the contact time between liquid and gas. The liquid phase is preferably sodium hydroxide solution of known concentration (not more than 2 N), enters from the top and the mixture of carbon dioxide and air (inert solvent for gaseous phase), enters from the bottom of the column. Thus we have counter-current flow column.NaOH is passed in excess of the theoretical requirement in the column.

<br>

The reaction which takes place in absorption column and the bubbling pot is:


![equation1](equation1.png)

<br>

When the samples from the absorption column and bubbling pot are titrated with HCl, first end-point is obtained using a Phenolphthalein indicator.

![equation1](equation2.png)

<br>

Moles of bicarbonate ion are equal to the moles of carbonate ion present. Also, the moles of H+ ion reacted is equal to the sum of moles of OH- and CO32- ions.
 

Second end-point is obtained using the Methyl Orange indicator which helps us in determining the total moles of bicarbonate ion (and hence the carbonate ions) present.

![equation1](equation3.png)