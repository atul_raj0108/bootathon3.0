var count : HTMLInputElement = <HTMLInputElement> document.getElementById("number");
var table : HTMLTableElement = <HTMLTableElement> document.getElementById("tab");

function mulTable() //function for constructing multiplication table
{
    var n : number= +(count.value);

    while(table.rows.length >= 1)
    {
        table.deleteRow(0);
    }


    for(let i=1; i <= n; i++)
    {
        var row : HTMLTableRowElement = table.insertRow();
        var cell1 : HTMLTableDataCellElement = row.insertCell(); // one cell for number
        var cell2 : HTMLTableDataCellElement = row.insertCell(); // next cell for printing *
        var cell3 : HTMLTableDataCellElement = row.insertCell(); // next cell for printing i
        var cell4 : HTMLTableDataCellElement = row.insertCell(); // next cell for printing =
        var cell5 : HTMLTableDataCellElement = row.insertCell(); // next cell for printing result
        
        cell1.innerHTML = n.toString() 
        cell2.innerHTML = " * ";
        cell3.innerHTML = i.toString();
        cell4.innerHTML = " = ";
        cell5.innerHTML = (n*i).toString();
    }
}