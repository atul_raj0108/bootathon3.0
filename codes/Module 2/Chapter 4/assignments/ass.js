var count = document.getElementById("number");
var table = document.getElementById("tab");
function mulTable() {
    var n = +(count.value);
    while (table.rows.length >= 1) {
        table.deleteRow(0);
    }
    for (let i = 1; i <= n; i++) {
        var row = table.insertRow();
        var cell1 = row.insertCell(); // one cell for number
        var cell2 = row.insertCell(); // next cell for printing *
        var cell3 = row.insertCell(); // next cell for printing i
        var cell4 = row.insertCell(); // next cell for printing =
        var cell5 = row.insertCell(); // next cell for printing result
        cell1.innerHTML = n.toString();
        cell2.innerHTML = " * ";
        cell3.innerHTML = i.toString();
        cell4.innerHTML = " = ";
        cell5.innerHTML = (n * i).toString();
    }
}
//# sourceMappingURL=ass.js.map