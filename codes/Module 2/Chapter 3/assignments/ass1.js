function area() {
    let Ax = document.getElementById("Ax");
    let Ay = document.getElementById("Ay");
    let Bx = document.getElementById("Bx");
    let By = document.getElementById("By");
    let Cx = document.getElementById("Cx");
    let Cy = document.getElementById("Cy");
    let Dx = document.getElementById("Dx");
    let Dy = document.getElementById("Dy");
    let ans = document.getElementById("ans");
    var x1 = parseFloat(Ax.value);
    var y1 = parseFloat(Ay.value);
    var x2 = parseFloat(Bx.value);
    var y2 = parseFloat(By.value);
    var x3 = parseFloat(Cx.value);
    var y3 = parseFloat(Cy.value);
    var x4 = parseFloat(Dx.value);
    var y4 = parseFloat(Dy.value);
    var AreaABC = (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2; //Area of Triangle ABC
    console.log(AreaABC);
    var AreaPAB = Math.abs((x1 * (y2 - y1) + x2 * (y4 - y1) + x4 * (y1 - y2)) / 2); // Area of Triangle PAB
    console.log(AreaPAB);
    var AreaPAC = Math.abs((x1 * (y4 - y3) + x4 * (y3 - y1) + x3 * (y1 - y4)) / 2); // Area of Triangle PAC
    console.log(AreaPAC);
    var AreaPBC = Math.abs((x4 * (y2 - y3) + x2 * (y3 - y4) + x3 * (y4 - y2)) / 2); // Area of Triangle PBC
    console.log(AreaPBC);
    var sum = AreaPAB + AreaPAC + AreaPBC; //Sum of areas of triangles PAB,PAC and PBC
    console.log(sum);
    if (Math.abs(AreaABC - sum) < 0.000001) //if True, Point lies inside
     {
        alert("Given point lies inside the Triangle");
    }
    else // if False, Point lies outside
     {
        alert("Given point doesn't lies inside the Triangle");
    }
}
//# sourceMappingURL=ass1.js.map